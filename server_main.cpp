#include<iostream>
#include "server.hpp"

Server::Server(boost::asio::io_context& io_context, short port)
    : acceptor_(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
      socket_(io_context)
{
    start_accept();
}

void Server::start_accept()
{
    acceptor_.async_accept(socket_,
        [this](const boost::system::error_code& error)
        {
            handle_accept(error);
        });
}

void Server::handle_accept(const boost::system::error_code& error)
{
    if (!error)
    {
        std::cout << "New client connected!" << std::endl;
    }
    start_accept();
}

