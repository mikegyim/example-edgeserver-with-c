#include "client.hpp"

#include <iostream>

Client::Client(boost::asio::io_context& io_context, const std::string& host, const std::string& port)
    : io_context_(io_context), socket_(io_context) {
    boost::asio::ip::tcp::resolver resolver(io_context_);
    boost::asio::ip::tcp::resolver::results_type endpoints = resolver.resolve(host, port);

    boost::asio::connect(socket_, endpoints);
}

void Client::write(const std::string& msg) {
    boost::asio::write(socket_, boost::asio::buffer(msg));
}

std::string Client::read() {
    boost::asio::streambuf buffer;
    boost::asio::read_until(socket_, buffer, "\n");

    std::string data = boost::asio::buffer_cast<const char*>(buffer.data());
    return data;
}

void Client::close() {
    socket_.close();
}

