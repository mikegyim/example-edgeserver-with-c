#include <iostream>
#include <boost/asio.hpp>
#include "client.hpp"

using boost::asio::ip::tcp;

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cerr << "Usage: client <host> <port>\n";
        return 1;
    }

    try {
        boost::asio::io_context io_context;
        Client client(io_context, argv[1], argv[2]);
        std::string message;
        std::cout << "Enter message: ";
        std::getline(std::cin, message);
        client.write(message);
        std::cout << "Response received: " << client.read() << std::endl;
        client.close();
    }
    catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << std::endl;
    }

    return 0;
}

