#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <boost/asio.hpp>

class Client {
public:
    Client(boost::asio::io_context& io_context, const std::string& host, const std::string& port);

    void write(const std::string& msg);

    std::string read();

    void close();

private:
    boost::asio::io_context& io_context_;
    boost::asio::ip::tcp::socket socket_;
};

#endif // CLIENT_HPP

