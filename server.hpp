#ifndef SERVER_HPP
#define SERVER_HPP

#include <boost/asio.hpp>

class Server
{
public:
    Server(boost::asio::io_context& io_context, short port);

private:
    void start_accept();
    void handle_accept(const boost::system::error_code& error);

    boost::asio::ip::tcp::acceptor acceptor_;
    boost::asio::ip::tcp::socket socket_;
};

#endif // SERVER_HPP

