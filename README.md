Install the Boost.Asio library

compile  the server side by typing the following command: 
g++ -o server server_main.cpp server.cpp -I/usr/local/boost/include -L/usr/local/boost/lib -lboost_system -lpthread

Start the server by running the following command:
./server 1234


compile  the client side by typing the following command:
g++ client_main.cpp client.cpp -o client -lboost_system -pthread

start the client by running the following command:

./client localhost 1234

You can then interact with the client and server by typing messages into the client terminal window and seeing the server's response in the server terminal window.

Please make sure to keep the server running while you interact with the client, as the server needs to be running on order for the client to connect to it.
